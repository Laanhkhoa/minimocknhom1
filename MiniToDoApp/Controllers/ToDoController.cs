﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MiniToDoApp.Model;
using MiniToDoApp.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniToDoApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ToDoController : ControllerBase
    {
        private readonly IToDoService _toDoService;

        public ToDoController(IToDoService toDoService)
        {
            _toDoService = toDoService;
        }

        // GET: api/<ToDotController>
        [HttpGet]
        public async Task<IEnumerable<ToDo>> GetAll()
        {
            return await _toDoService.GetAllToDo();
        }

        // GET api/<ToDotController>/
        [HttpGet("{id}")]
        public async Task<ToDo> GetById(int id)
        {
            return await _toDoService.GetToDoById(id);
        }

        // POST api/<ToDotController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ToDo todo)
        {
            try
            {
                await _toDoService.InsertToDo(todo);
                return Ok(todo);
            }
            catch
            {
                return NoContent();
            }
        }

        // PUT api/<ToDotController>/
        [HttpPut("{id}")]
        public async Task<bool> Put(int id, [FromBody] ToDo todo)
        {
            return await _toDoService.UpdateToDo(todo);
        }

        // DELETE api/<ToDotController>/
        [HttpDelete("{id}")]
        public async Task<bool> Delete(int id)
        {
            var todo = await _toDoService.GetToDoById(id);
            return await _toDoService.DeleteToDo(todo);
        }
    }
}
