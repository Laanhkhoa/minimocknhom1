﻿using MiniToDoApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniToDoApp.Service
{
        public interface IToDoService
        {
            Task<IEnumerable<ToDo>> GetAllToDo();
            Task<ToDo> GetToDoById(int id);
            Task<bool> UpdateToDo(ToDo todo);
            Task<bool> DeleteToDo(ToDo todo);
            Task<bool> InsertToDo(ToDo todo);

        }
}
