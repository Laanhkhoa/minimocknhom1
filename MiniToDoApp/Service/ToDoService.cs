﻿using Microsoft.EntityFrameworkCore;
using MiniToDoApp.Model;
using MiniToDoApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniToDoApp.Service
{
    public class ToDoService : IToDoService
    {
        private readonly IToDoRepository _toDoRepository;

        public ToDoService(IToDoRepository toDoRepository)
        {
            _toDoRepository = toDoRepository;
        }

        public async Task<bool> DeleteToDo(ToDo todo)
        {
            try
            {
                await _toDoRepository.Delete(todo);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<IEnumerable<ToDo>> GetAllToDo()
        {
            return await _toDoRepository.SelectAll().ToListAsync();
        }

        public async Task<ToDo> GetToDoById(int id)
        {
            return await _toDoRepository.SelectById(id);
        }

        public async Task<bool> InsertToDo(ToDo todo)
        {
            try
            {
                await _toDoRepository.Insert(todo);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> UpdateToDo(ToDo todo)
        {
            try
            {
                await _toDoRepository.Update(todo);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
