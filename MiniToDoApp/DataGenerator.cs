﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MiniToDoApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniToDoApp
{
    public class DataGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ToDoContext(
                serviceProvider.GetRequiredService<DbContextOptions<ToDoContext>>()))
            {
                // Look for any board games.
                if (context.ToDos.Any())
                {
                    return;   // Data was already seeded
                }

                context.ToDos.AddRange(
                    new ToDo
                    {
                        Id = 1,
                        Name = "Chi Thao dep gai do something"
                    },
                    new ToDo
                    {
                        Id = 2,
                        Name = "Chi Linh dep gai"
                    });

                context.SaveChanges();
            }
        }
    }
}
