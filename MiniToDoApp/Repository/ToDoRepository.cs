﻿using Microsoft.EntityFrameworkCore;
using MiniToDoApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniToDoApp.Repository
{
    public class ToDoRepository : IToDoRepository
    {
        private ToDoContext _db;
        private DbSet<ToDo> _table;
        public ToDoRepository(ToDoContext db)
        {
            this._db = db;
            _table = _db.Set<ToDo>();
        }

        public IQueryable<ToDo> SelectAll()
        {
            return _db.Set<ToDo>();
        }

        public async Task<ToDo> SelectById(int id)
        {
            try
            {
                return await _table.FindAsync(id);
            }
            catch
            {
                return null;
            }
        }

        public async Task Insert(ToDo obj)
        {
            await _table.AddAsync(obj);
            await _db.SaveChangesAsync();
        }

        public async Task Update(ToDo obj)
        {
            /*_table.Attach(obj)*/
            _db.Entry(obj).State = EntityState.Modified;
            await _db.SaveChangesAsync();
        }

        public async Task Delete(ToDo obj)
        {
            _table.Remove(obj);
            await _db.SaveChangesAsync();
        }
    }
}
