﻿using MiniToDoApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MiniToDoApp.Repository
{
    public interface IToDoRepository 
	{
		IQueryable<ToDo> SelectAll();
		Task<ToDo> SelectById(int id);
		Task Insert(ToDo obj);
		Task Update(ToDo obj);
		Task Delete(ToDo obj);
	}
}
