﻿using Microsoft.EntityFrameworkCore;
using MiniToDoApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniToDoApp
{
    public class ToDoContext : DbContext
    {
        public ToDoContext(DbContextOptions<ToDoContext> options)
        : base(options) { }

        public DbSet<ToDo>  ToDos { get; set; }
    }
}
